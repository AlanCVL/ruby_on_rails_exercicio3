class AddCommentToLikeComment < ActiveRecord::Migration[5.2]
  def change
    add_reference :like_comments, :comment, foreign_key: true
  end
end
