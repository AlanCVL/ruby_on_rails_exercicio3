class AddUserToLikeComment < ActiveRecord::Migration[5.2]
  def change
    add_reference :like_comments, :user, foreign_key: true
  end
end
