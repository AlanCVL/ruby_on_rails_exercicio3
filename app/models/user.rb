class User < ApplicationRecord
    validates :email, :name, :password, presence: true
    validates :email, uniqueness: true
    #se relaciona apenas uma vez com user é belong_to e relaciona muitas vezes é has_many
    has_many :posts #optional :true
    has_many :comments
    has_many :likes
    has_many :like_comments

    has_many :guest_invite, foreign_key: :host_id, class_name: 'Invite'
    has_many :host_invite, foreign_key: :guest_id, class_name: 'Invite'

    has_many :guests, through: :guest_invite
    has_many :hosts, through: :host_invite

    has_secure_password #rails espera q vc tenha password_digest q ja faz td cript para a gente

    enum kind: {
        admin: 0,
        standard: 1
    }

    def age
        ((Date.today - birthdate)/365).to_i
    end

end
