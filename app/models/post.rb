class Post < ApplicationRecord
    validates :edit, presence: true
    # Pertence a user
    belongs_to :user
    has_many :comments
    has_many :likes
    
end
