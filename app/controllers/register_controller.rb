class RegisterController < ApplicationController

  def sign_up
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end


  private
   def user_params
      params.require(:user).permit(:name, :email, :gender, :birthdate, :password, :password_confirmation,:post_id , :comment_id,  habit_ids: [])
    end

end
