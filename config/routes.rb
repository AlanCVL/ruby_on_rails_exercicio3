Rails.application.routes.draw do
  get '/current_user', to: 'application#user_must_exist'
  post  '/login', to: 'session#login'
  post '/sign_up', to: 'register#sign_up'
  resources :like_comments
  resources :likes
  resources :comments
  get '/posts/:post_id/users', to: 'users#index'
  get '/comments/:comment_id/users', to: 'users#index_comment'
  resources :posts
  resources :users, except: [:create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
